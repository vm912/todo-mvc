import React, { useState, useEffect } from "react";
import classNames from "classnames";
import {showing} from "../App";

const Footer = ({listShowing, onListFilterChange, activeCount, completedCount,  onClearCompleted}) => {

    const [clearEnabled, setClearEnabled] = useState(false);



    useEffect(() => {
        setClearEnabled(completedCount !== 0);
    }, [completedCount]);

    
    const handleOnClearCompleted = () => {
    
        setClearEnabled(false);
        onClearCompleted();
    }

    const handleListChange = (e) => {
        e.preventDefault(); 
        onListFilterChange(e.currentTarget.getAttribute('filter'));
    }

    const itemWord = () => {
        return (activeCount === 1) ? "item" : "items";
    }



    let clearButton = (clearEnabled) ? (<button
        className="clear-completed"
        onClick= {handleOnClearCompleted}>
        Clear completed
    </button>) : null;

   

    return (
          <footer className="footer">
          <span className="todo-count">
                <strong>{activeCount}</strong> {itemWord()} left
			</span>
            <ul className="filters">
                <li>
                <a
								href="#/"
                                className={classNames({selected: (listShowing === showing.ALL_TODOS)})}
                                onClick={handleListChange}
                                filter={showing.ALL_TODOS}
                                >
									All 
							</a>
                </li>
                {' '}
                <li>
                <a
								href="#/active"
                                className={classNames({selected: (listShowing === showing.ACTIVE)})}
                                onClick={handleListChange}
                                filter={showing.ACTIVE}

                                >
									Active 
							</a>
                </li>
                {' '}
                <li>
                <a
								href="#/completed"
                                className={classNames({selected: (listShowing === showing.COMPLETED)})}
                                onClick={handleListChange}
                                filter={showing.COMPLETED}

                                >
									Completed 
							</a>
                </li>

            </ul>
            {clearButton}
          </footer>
    )
}

export default Footer;