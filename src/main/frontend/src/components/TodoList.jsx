import React from "react";
import TodoItem from "./TodoItem";


const TodoList = ({todoList, editedTodo, setEditedTodo, deleteATodo, editATodo, toggleCompleted, currentlyEditing, setCurrentlyEditing}) => {


        return (
        <div className="todo-list-containter">
            <ul className="todo-list">
                {todoList.map((todo, index) => {

                    return (
                            <TodoItem key= {index} todo={todo}
                            editedTodo = {editedTodo}
                            setEditedTodo = {setEditedTodo}
                            deleteATodo={deleteATodo}
                            editATodo={editATodo}  
                            toggleCompleted={toggleCompleted} 
                            />
                    )
                })}
            </ul>
        </div>
    ) 

}

export default TodoList;