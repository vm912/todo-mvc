import React, { useState, useEffect } from "react";
import classNames from "classnames";
import {ESCAPE_KEY,ENTER_KEY} from "../App";


const TodoItem = ({todo, editedTodo, setEditedTodo,  deleteATodo, editATodo, toggleCompleted}) => {
    
    const[currentlyEditing, setCurrentlyEditing] = useState(false);

    const[submitAllowed, setSubmitAllowed] = useState(true);

    const myRef = React.createRef();

    useEffect(
        () => {
                setEditedTodo(todo);
                setSubmitAllowed(true);
            }, [todo]
    );

    useEffect(
        () => {
            if(currentlyEditing){
                const node = myRef.current;
				node.focus();
                node.setSelectionRange(node.value.length, node.value.length);
                setSubmitAllowed(true);

            }
        }, [currentlyEditing]
    );
    
    const handleDelete = () => {
        deleteATodo(todo.id);
    
    }

    const handleInputChange = (e) => {
        e.preventDefault();
        setEditedTodo({...editedTodo, message: e.target.value});
    }

    const handleKeyDown = (e) => {
        if (e.which === ESCAPE_KEY) {
            setSubmitAllowed(false);
            handleCancel(e);
        } else if (e.which === ENTER_KEY) {
            handleSubmit(e);
            setSubmitAllowed(false);
        }
    }

    const handleSubmit= (e) => {
        e.preventDefault();
        if(submitAllowed){
            editATodo(editedTodo);
        }
        setCurrentlyEditing(false);
    }

    const handleCancel = (e) => {
        e.preventDefault();
        setEditedTodo(todo);
        setCurrentlyEditing(false);

    } 

    const handleCompletedToggle = () => {
        toggleCompleted(todo.id, !todo.completed);
    }

    const handleEdit = () => {
        setEditedTodo(todo);
        setCurrentlyEditing(true);
    }

    return (
            <li className= {classNames({
                completed: todo.completed,
                editing: currentlyEditing,
                message: !currentlyEditing 
            })}> 
                <div className="view">
                    <input
                        className="toggle"
                        type="checkbox"
                        //double '!' for the auto switch to controlled component (initial value is null, so it converts it to undefined/false)
                        checked={!!todo.completed}
                        onChange={handleCompletedToggle}
                    />           
                    <label onDoubleClick={handleEdit}>
                                {todo.message}
                    </label>
                    <button className="remove-button" onClick={handleDelete}></button>
                </div>
                <input
                            ref={myRef}
                            className= "edit"
                            type="text"
                            //operator || used to set the value initially if not set (convert component to controlled)
                            value={editedTodo.message || ''}
                            onChange={handleInputChange}
                            autoComplete="off"
                            onBlur={handleSubmit}
                            evtname = 'onBlur'
                            onKeyDown={handleKeyDown}
                    />
            </li>
    )
}

export default TodoItem;