import React, { useState, useEffect } from "react";
import {ENTER_KEY} from "../App";


const TodoForm = ({todo, setTodo, listLength, createTodo, activeCount, toggleAllToCompleted}) => {

        //TODO FIX THE TOGGLE ALL STILL APPEARING AFTER CLEAR COMPLETED


    const[toggleAllShowing, setToggleAllShowing] = useState(false);

    const[checkboxChecked, setCheckboxChecked] = useState(false);

    //updating the showing boolean value
    useEffect(() => {
        setToggleAllShowing(listLength !== 0);
    },[listLength] ); 

    //changing the checkbox checked value
    useEffect(() => {
        setCheckboxChecked(activeCount === 0);
    },[activeCount] ); 


    const handleInputChange = (e) => {
        e.preventDefault();
        setTodo({...todo, message: e.target.value});
    }

    const handleCheckboxChange = () => {
        setCheckboxChecked(!checkboxChecked);
        toggleAllToCompleted(!(activeCount === 0));
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        createTodo(todo);
        //update the todo from the backend before adding to list to get the id
                 setTodo({id:"", message: "", completed: false});

    }

    const handleKeyDown = (e) => {
        if (e.which === ENTER_KEY) {
            handleSubmit(e);
        }


    }


    let toggleAll= toggleAllShowing ? (
        <section className="main">
            <input
                id="toggle-all"
                className="toggle-all"
                type="checkbox"
                onChange={handleCheckboxChange}
                checked={checkboxChecked}
                >
            </input>
            <label
                        htmlFor="toggle-all"
                    />
        </section>
    ) : null;

    
    return (
        <div className="todo-form-containter">
                {toggleAll}

                <input
                    className="new-todo-input"
                    type="text"
                    autoComplete="off"
                    placeholder="What needs to be done?"
                    value={todo.message}
                    onChange={handleInputChange}
                    autoFocus={true}
                    onKeyDown={handleKeyDown}

                />
        </div>
    )
}

export default TodoForm;