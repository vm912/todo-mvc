import React,{useState, useEffect} from 'react';
import './App.css';
import axios from 'axios';
import TodoList from './components/TodoList';
import TodoForm from './components/TodoForm';
import Footer from './components/Footer';



export const showing = { ALL_TODOS: "all",
                             ACTIVE: "active",
                              COMPLETED: "completed"};

export const[ESCAPE_KEY, ENTER_KEY]  = [27,13]; 

function App() {


  const [todoList, setTodoList] = useState([]);

  const[fullList, setFullList] = useState([]);

  const [listShowing, setListShowing] = useState(showing.ALL_TODOS);
  
  const[todo, setTodo] = useState({
    id: "",
    message: "",
    completed: false
  });

  const[editedTodo, setEditedTodo] = useState({
    id: "",
    message: "",
    completed: false
  });

  const [activeCount, setActiveCount] = useState([0]);
  
  const [completedCount, setCompletedCount] = useState([0]);

  const fetchActiveTodos = () => {
    axios.get("http://localhost:8080/api/todo/active").then( res => {
        setTodoList(res.data);
    });
  }

  const fetchCompletedTodos = () => {
    axios.get("http://localhost:8080/api/todo/completed").then( res => {
        setTodoList(res.data);
    });
  }

  const createTodo = (todo) => {
    let headers = {
      'Content-Type': 'application/json'
    };
     axios.post("http://localhost:8080/api/todo", JSON.stringify({message: todo.message}), {headers}).then(() => {
      updateFilteredList();
    });
  };

  const deleteATodo = (id) => {
   
     axios.delete(`http://localhost:8080/api/todo/${id}`).then(() => {
      updateFilteredList();
    });
  };

  const editATodo = (todo) => {
    let headers = {
      'Content-Type': 'application/json'
    };
     axios.put(`http://localhost:8080/api/todo/message/${todo.id}`, JSON.stringify({message: todo.message}), {headers}).then(() => {
      updateFilteredList();

    });
  }

  //clear all completed from the list and update backend
  const onClearCompleted =  () => {

     axios.delete(`http://localhost:8080/api/todo/completed`).then(() => {
      updateFilteredList();

    });
  }

  const toggleCompleted =  (id, updatedCompleted) => {
   
    //http put method
     axios.put(`http://localhost:8080/api/todo/toggle-status/${id}`).then(() => {
      updateFilteredList();
    });

  };

  
  const toggleAllToCompleted =  (completedChecked) => {
     axios.put(`http://localhost:8080/api/todo/toggle-status/all`, null, { params: {
      completedChecked
    }}).then(() => {
      updateFilteredList();

    });

  };

  const onListFilterChange = (filter) => {
    setListShowing(filter);

  }

  const updateFilteredList = async () => {

    await axios.get("http://localhost:8080/api/todo").then( res => {
      setFullList(res.data);
    });


    switch(listShowing){
      case 'all':
        await axios.get("http://localhost:8080/api/todo").then( res => {
        setFullList(res.data);
        setTodoList(res.data);
        });
        break;
      case 'active':
        fetchActiveTodos();
        break;
      case 'completed':
        fetchCompletedTodos();
        break;
      default:
        break;
    }
    
  }

  //update counters based on the UPDATED full list
  const updateCounters = () => {
    setActiveCount(fullList.filter((todo)=> {
      return todo.completed === false;
    }).length);
    setCompletedCount(fullList.filter((todo)=> {
      return todo.completed === true;
    }).length);
  }


  //triggers only on mount
  useEffect(() => {
      updateFilteredList();
    }, []);


  //update counters based on the full list change 
  useEffect(() => {
    updateCounters();

  }, [fullList]);

  //update todo list based on the filter
  useEffect(() => {
    updateFilteredList();
  }, [listShowing]);



  return (
    <section className="todoapp">
          <header className="header">
            <h1 className="title">todos</h1>
            <TodoForm todo={todo} 
            setTodo={setTodo} 
            todoList={todoList} 
            setTodoList={setTodoList} 
            listLength={activeCount+completedCount}
            activeCount={activeCount}
            toggleAllToCompleted={toggleAllToCompleted}
            createTodo={createTodo} 
            >
            </TodoForm>
          </header>
          <TodoList todoList={todoList} 
          editedTodo = {editedTodo}
          setEditedTodo={setEditedTodo}
          deleteATodo={deleteATodo} 
          editATodo={editATodo}
          toggleCompleted={toggleCompleted}> 
          </TodoList>
          <Footer 
          listShowing={listShowing}
          onListFilterChange={onListFilterChange}
          activeCount={activeCount}
          completedCount={completedCount}
          setCompletedCount={setCompletedCount}
          onClearCompleted={onClearCompleted}></Footer>
    </section>
  );
}

export default App;
