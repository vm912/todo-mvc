package com.example.todoapp.repository;


import com.example.todoapp.model.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TodoMessageRepository extends JpaRepository<Todo, Long> {

    List<Todo> findAllByCompleted(boolean isCompleted);

    void deleteAllByCompleted(boolean isCompleted);

}
