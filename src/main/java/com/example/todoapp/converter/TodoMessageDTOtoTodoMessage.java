package com.example.todoapp.converter;

import com.example.todoapp.model.Todo;
import com.example.todoapp.model.dto.TodoMessageDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class TodoMessageDTOtoTodoMessage implements Converter<TodoMessageDTO, Todo> {

    @Nullable
    @Override
    public Todo convert(TodoMessageDTO todoMessageDTO) {

        return (todoMessageDTO == null) ?
                null : Todo.builder().message(todoMessageDTO.getMessage()).completed(false).build();

    }
}
