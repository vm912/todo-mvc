package com.example.todoapp.controller;

import com.example.todoapp.model.Todo;
import com.example.todoapp.model.dto.TodoMessageDTO;
import com.example.todoapp.service.TodoMessageService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/todo")
@CrossOrigin("*")
public class TodoMessageController {

    private final TodoMessageService todoMessageService;

    public TodoMessageController(TodoMessageService todoMessageService) {
        this.todoMessageService = todoMessageService;
    }

    @GetMapping
    public List<Todo> listAllTodos(){
        return todoMessageService.getAllTodoMessages();
    }

    @GetMapping("/{messageId}")
    public ResponseEntity<Todo> getTodoMessageById(@PathVariable Long messageId){
        System.out.println("one");
        Todo todoMessage = todoMessageService.fetchTodoMessage(messageId);
        return new ResponseEntity<>(todoMessage, HttpStatus.OK);
    }

    @GetMapping("/active")
    public List<Todo> listAllActiveTodos(){
        return  todoMessageService.getAllActiveTodoMessages();
    }

    @GetMapping("/completed")
    public List<Todo> listAllCompletedTodos(){
        return  todoMessageService.getAllCompletedTodoMessages();
    }

    @PostMapping
    public ResponseEntity<Todo> createTodoMessage(@RequestBody TodoMessageDTO todoDTO){
            return new ResponseEntity<>(todoMessageService.createTodoMessage(todoDTO), HttpStatus.CREATED);
    }

    @DeleteMapping("/{messageId}")
    public ResponseEntity<Object> deleteTodoMessage(@PathVariable Long messageId){
        todoMessageService.deleteTodoMessage(messageId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/toggle-status/{messageId}")
    public ResponseEntity<Todo> updateStatus(@PathVariable Long messageId){
        Todo updatedTodo = todoMessageService.updateSingleTodoStatus(messageId);
        return new ResponseEntity<>(updatedTodo, HttpStatus.OK);
    }

    @PutMapping("/message/{messageId}")
    public ResponseEntity<Todo> updateTodoMessage(@PathVariable Long messageId, @RequestBody TodoMessageDTO todoMessageDTO){
        Todo updatedTodo = todoMessageService.updateTodoMessage(messageId, todoMessageDTO);
        return new ResponseEntity<>(updatedTodo, HttpStatus.OK);
    }

    @DeleteMapping("/completed")
    public ResponseEntity<Object> clearCompletedTodos(){
        todoMessageService.clearCompletedTodoMessages();
        return ResponseEntity.ok().build();
    }


    @PutMapping("/toggle-status/all")
    public ResponseEntity<List<Todo>> toggleAllStatuses(@RequestParam boolean completedChecked){
        List<Todo> updatedTodos =  todoMessageService.toggleAllStatuses(completedChecked);
        return ResponseEntity.ok(updatedTodos);
    }



}
