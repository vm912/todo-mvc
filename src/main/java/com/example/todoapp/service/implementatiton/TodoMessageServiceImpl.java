package com.example.todoapp.service.implementatiton;

import com.example.todoapp.converter.TodoMessageDTOtoTodoMessage;
import com.example.todoapp.model.Todo;
import com.example.todoapp.model.dto.TodoMessageDTO;
import com.example.todoapp.repository.TodoMessageRepository;
import com.example.todoapp.service.TodoMessageService;
import com.example.todoapp.service.exception.EntityMissingException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TodoMessageServiceImpl implements TodoMessageService {

    private final TodoMessageRepository todoMessageRepository;
    private final TodoMessageDTOtoTodoMessage messageDTOConverter;

    public TodoMessageServiceImpl(TodoMessageRepository todoMessageRepository, TodoMessageDTOtoTodoMessage messageDTOConverter) {
        this.todoMessageRepository = todoMessageRepository;
        this.messageDTOConverter = messageDTOConverter;
    }

    @Override
    public List<Todo> getAllTodoMessages() {
        return todoMessageRepository.findAll();
    }

    @Override
    public List<Todo> getAllActiveTodoMessages() {
        return todoMessageRepository.findAllByCompleted(false);
    }

    @Override
    public List<Todo> getAllCompletedTodoMessages() {
        return todoMessageRepository.findAllByCompleted(true);
    }

    @Override
    public Todo createTodoMessage(TodoMessageDTO todoMessageDTO) {
        Todo convertedMessage = messageDTOConverter.convert(todoMessageDTO);
        if (convertedMessage != null) {
            return todoMessageRepository.save(convertedMessage);
        } else {
            throw new RuntimeException("Given todo message must not be null!");
        }
    }

    @Override
    public void deleteTodoMessage(Long messageId) {
        Todo messageToDelete = fetchTodoMessage(messageId);
        todoMessageRepository.delete(messageToDelete);
    }

    @Override
    public Todo fetchTodoMessage(Long messageId) {
        return findTodoMessageById(messageId)
                .orElseThrow(() -> new EntityMissingException(Todo.class, messageId)
                );
    }

    @Transactional
    @Override
    public void clearCompletedTodoMessages() {
        todoMessageRepository.deleteAllByCompleted(true);
    }

    @Override
    public Todo updateSingleTodoStatus(Long messageId) {
        Todo todoToUpdate = fetchTodoMessage(messageId);
        todoToUpdate.setCompleted(!todoToUpdate.isCompleted());
        return todoMessageRepository.save(todoToUpdate);
    }

    @Override
    public Todo updateTodoMessage(Long messageId, TodoMessageDTO todoDTO) {
        Todo todoToUpdate = fetchTodoMessage(messageId);
        todoToUpdate.setMessage(todoDTO.getMessage());
        return todoMessageRepository.save(todoToUpdate);
    }

    @Override
    public List<Todo> toggleAllStatuses(boolean completedChecked) {

        List<Todo> currentTodos = getAllTodoMessages();
        return currentTodos.stream().map(todo -> {
            boolean currentlyCompleted = todo.isCompleted();
            if(!currentlyCompleted &&  completedChecked){
                todo.setCompleted(true);
            } else if(currentlyCompleted && !completedChecked){
                todo.setCompleted(false);
            }
            return todoMessageRepository.save(todo);
        }).collect(Collectors.toList());


    }


    private Optional<Todo> findTodoMessageById(Long messageId){
        return todoMessageRepository.findById(messageId);
    }

}
