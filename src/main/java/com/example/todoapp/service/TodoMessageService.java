package com.example.todoapp.service;

import com.example.todoapp.model.Todo;
import com.example.todoapp.model.dto.TodoMessageDTO;

import java.util.List;

public interface TodoMessageService {


    List<Todo> getAllTodoMessages();

    List<Todo> getAllActiveTodoMessages();

    List<Todo> getAllCompletedTodoMessages();

    Todo createTodoMessage(TodoMessageDTO todoDTO);

    void deleteTodoMessage(Long messageId);

    Todo fetchTodoMessage(Long messageId);

    void clearCompletedTodoMessages();

    Todo updateSingleTodoStatus(Long messageId);

    Todo updateTodoMessage(Long messageId, TodoMessageDTO todoDTO);

    List<Todo> toggleAllStatuses(boolean completedChecked);

}
